import numpy as np
import torch

from EMCqMRI.core.configuration import pll_configuration
from EMCqMRI.core.engine import build_model as core_build
from EMCqMRI.core.engine import train_model

import pickle
import matplotlib.pyplot as plt


def override_model(configObject):

    from custom_dataset import denoise_dataset
    configObject.args.engine.dataset = denoise_dataset.DatasetModel(configObject)

    from custom_inference_model.rim_stride import rim
    configObject.args.engine.inference_model = rim.Rim(configObject)

    from custom_likelihood_model import gaussian
    configObject.args.engine.likelihood_model = gaussian.Gaussian(configObject)

    from custom_signal_model import denoise
    configObject.args.engine.signal_model = denoise.Denoise(configObject)




if __name__=='__main__':
    configurationObj = pll_configuration.Configuration('training')
    override_model(configurationObj)
    core_build.make(configurationObj)
    
    # logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))

    # configurationObj.args.engine.trainer.log_training_fun = custom_log_fun

    for i, sample in enumerate(configurationObj.args.engine.dataloader):
        # print(sample)
        snr = (torch.mean(sample['image'])/configurationObj.args.task.sigmaNoise).detach().numpy()
        print('snr: ', snr)
        
        data = {}
        data['signal'] = sample['image']
        data['label'] = sample['label']
        data['snr'] = snr
        data['frequency'] = configurationObj.args.task.frequency
        data['filename'] = sample['filename']

        save_path = '../evaluation/data/snr_30_freq_7/'
        # print(sample['filename'])
        with open(save_path + sample['filename'][0] + '.pkl', 'wb') as f:
            pickle.dump(data, f)
            print("data saved to : ", f)


        # t = np.linspace(0,len(sample['image'][0,0]), len(sample['image'][0,0]))
        # plt.figure()
        # plt.plot(t, sample['image'][0,0])
        # plt.show()


    # logging.info('Starting training...')
    # configurationObj.args.engine.trainer.run()
