from mlxtend.data import loadlocal_mnist
import platform
import numpy as np
import matplotlib.pyplot as plt

X, y = loadlocal_mnist(
            images_path='../../dataset/train-images-idx3-ubyte', 
            labels_path='../../dataset/train-labels-idx1-ubyte')

image_1 = X[220]
reshape_image_1 = np.reshape(image_1, [28, 28])

plt.figure()
plt.imshow(reshape_image_1)
plt.show()

print('Dimensions: %s x %s' % (X.shape[0], X.shape[1]))
# print('\n1st row', X[0])

