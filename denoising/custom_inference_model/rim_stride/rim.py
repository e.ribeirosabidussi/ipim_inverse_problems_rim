from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from .rnn import ConvRNN
from core.base import base_inference_model
from core.utilities.core_utilities import ProgressBarWrap
import torch
from torch.autograd import Variable
import torch.nn as nn


class Rim(base_inference_model.InferenceModel, nn.Module):
    """
        Class Implementing the RIM model.
        Methods:
            - setOpts
                inputs: a Dict containing the key and value for a new configuration setting
            - forward
                inputs: signal (measured signal); args (options containing, at least args.batchSize and args.device)
                outputs: Estimated parameters
    """

    def __init__(self, configObject):
        super(Rim, self).__init__()
        self.__name__ = 'RIM'
        self.__require_initial_guess__ = True
        self.args = configObject.args
        self.__buildNetwork__()

    def __buildNetwork__(self):
        """
            Hidden method that instanciates a version of the RNN module based on the configuration file
        """
        self.rnn = ConvRNN(2, conv_dim=1)

    def __initHidden__(self, signal):
        """
            Initialises all hidden states in the network
        """
        shape_input = torch.tensor((signal.shape)[2:])
        shape_hs1 = [1, self.args.engine.batchSize, self.args.inference.outputChannelsLayer2]
        st_1 = Variable(torch.zeros(tuple(shape_hs1)).to(device=self.args.engine.device))
        return st_1

    def __getGradients__(self, signal, kappa):
        """
            Compute and return gradients of the Likelihood Function w.r.t. parameter maps
        """

        gradientParamBatch = []
        for batch in range(len(signal)):
            clonedMaps = Variable(kappa[batch].clone(), requires_grad=True)
            gradientParamBatch.append(self.args.engine.likelihood_model.gradients(signal[batch], clonedMaps))
        
        paramGrad = torch.stack(gradientParamBatch)
        paramGrad[torch.isnan(paramGrad)] = 0
        paramGrad[torch.isinf(paramGrad)] = 0
        return paramGrad

    @ProgressBarWrap
    def update_bar(self, loss, args):
        return -1

    def forward(self, inputs):
        signal = inputs
        kappa = self.args.engine.signal_model.initializeParameters(signal)
        hidden = None
        estimates = []

        for _ in range(self.args.inference.inferenceSteps):
            paramGrad = self.__getGradients__(signal, kappa)
            input = torch.cat([kappa, paramGrad], 1)

            dx, hidden = self.rnn.forward(input, hidden)
            kappa = kappa + dx

            estimates.append(kappa)

        return torch.stack(estimates)
