# -*- coding: utf-8 -*-
"""
Created on Thu May  6 09:28:38 2021

@author: 20175769
"""
#####################################################################################
# to denoise 1 sample

import numpy as np
import numpy.linalg as alg
import numpy.random as rand
from scipy.sparse import diags
import matplotlib.pyplot as plt

n = 1000    # grid points
k = 20       # frequency           
s = 0.5    # noise 

x = np.linspace(0,1,n)
alpha = 10000

f = np.sin(k*x) + rand.normal(0,s,n)
b = f

b[0] = b[0]/2
b[n-1] = b[n-1]/2

T = diags([1/6, 2/3, 1/6], [-1, 0, 1], shape=(n, n)).toarray()
T[0,0] = 1/3
T[n-1,n-1] = 1/3

S = diags([-1, 2, -1], [-1, 0, 1], shape=(n, n)).toarray()
S[0,0] = 1
S[n-1,n-1] = 1

A = T + alpha * S
u = alg.solve(A, b)

plt.plot(x,f)
plt.plot(x,u)
plt.plot(x, np.sin(k*x), c='k')
plt.legend(['f_delta', 'solution', 'ground_truth'])
plt.show()

#####################################################################################
# to optimize alpha 

import numpy.matlib as mat
import numpy as np
import numpy.random as rand
import matplotlib.pyplot as plt
import time
import numpy.linalg as alg
from scipy.sparse import diags

##### Functions ######

def Inverse2(alpha, n):     # compute inverse FEM matrix. Note: w.r.t. reprt, S and T are interchanged
    T = diags([1/6, 2/3, 1/6], [-1, 0, 1], shape=(n, n)).toarray()
    T[0,0] = 1/3
    T[n-1,n-1] = 1/3
    
    S = diags([-1, 2, -1], [-1, 0, 1], shape=(n, n)).toarray()
    S[0,0] = 1
    S[n-1,n-1] = 1

    A = T + alpha * S
    return alg.inv(A)
    
def r_alpha(alpha, n, Ground, Noisy):         #function to optimize
    return alg.norm(Inverse2(alpha, n) @ Noisy - Ground)**2


##### Settings ######
n = 1000               #grid size
k_dim = 100           #nbr frequency values
s_dim = 100           #nbr of noise levels
max_iter = 500        #max nbr of iterations
step = 0.1            #Newton step size
alpha = 18            #initial guess alpha
error_tol = 1e-5      #stop iteration when update less than error_tol
h = 1e-5              #derivative accuracy

k = np.linspace(1, 20, k_dim)*np.pi    #list with frequency values
s = np.linspace(0.1, 0.3, s_dim)       #list with noise levels


###### Data #####
K = np.reshape(mat.repmat(k,1,s_dim), [1,k_dim*s_dim])
S = np.reshape(np.repeat(s, [k_dim]*s_dim), [1,k_dim*s_dim])
X = np.reshape(np.linspace(0,1,n), [n,1])

Ground = np.sin(X @ K)
Noisy = Ground + np.multiply(np.reshape(rand.normal(0,1,n*k_dim*s_dim), [n, k_dim*s_dim]), mat.repmat(S,n,1))
Noisy[0,:] = Noisy[0,:] / 2
Noisy[n-1,:] = Noisy[n-1,:] / 2


###### Newton Iteration ######
start_time = time.time()
error = 1
count_iter = 0
while (count_iter < max_iter) & (error > error_tol):
    r_plus = r_alpha(alpha+h, n, Ground, Noisy)
    r_min = r_alpha(alpha-h, n, Ground, Noisy)
    alpha_new = alpha - h/2*step* (r_plus - r_min) / abs(r_plus -2*r_alpha(alpha, n, Ground, Noisy) + r_min)
    error = abs(alpha_new - alpha)
    alpha = alpha_new
    count_iter += 1
    print(alpha, count_iter)   #displays progress
    
# display elpased time
print("--- %s seconds ---" % (time.time() - start_time))

# display peformance of found alpha
solution = Inverse2(alpha, n) @ Noisy - Ground
norms = np.sqrt((solution**2).sum(axis=0))
print('Bias', np.mean(norms))
print('Variance', np.var(norms))
print('Standard Deviation', np.var(norms)**(1/2))

plt.hist(norms)
###############################################################################################
# plot of alpha

G = []  
for i in np.linspace(1, 100, 50):
    G += [r_alpha(i, n, Ground, Noisy)]
    print(i)
    
plt.plot(np.linspace(1,100, 50), G)

#############################################################################################


    




    
    