from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from core.base import base_inference_model
from core.utilities.core_utilities import ProgressBarWrap
import torch

import matplotlib.pyplot as plt
import numpy as np

class Mle(base_inference_model.InferenceModel, object):
    """
    """
    def __init__(self, configObject):
        super(Mle, self).__init__()
        self.__name__ = 'MLE'
        self.__require_initial_guess__ = True
        self.args = configObject.args

    @ProgressBarWrap
    def update_bar(self, loss, args):
        return -1

    def forward(self, inputs):
        signal = inputs
        t = np.linspace(0, len(signal[0,0]), len(signal[0,0]))
        kappa = self.args.engine.signal_model.initializeParameters(signal)
        kappa_ = torch.zeros_like(kappa)
        for s in range(self.args.inference.inferenceSteps):
            estimates = self.args.engine.signal_model.forwardModel(kappa)
            loss = self.args.engine.likelihood_model(signal, estimates)
            kappa = self.args.engine.optimizer.forward(kappa, loss)

            if torch.isnan(loss):
                print("Loss diverged")
                break
            
            if len(np.shape(signal[0,0]))>1:
                if (s%1000 == 0) or (s%2999 == 0):
                    fig, ax = plt.subplots(1,3)
                    ax[0].imshow(signal[0,0], vmin=torch.min(signal), vmax=torch.max(signal))
                    ax[1].imshow(kappa[0,0].detach().numpy(), vmin=torch.min(signal), vmax=torch.max(signal))
                    ax[2].plot(signal[0,0], kappa[0,0].detach().numpy(), 'xk')
                    plt.show()   
            else:
                if (s%1000 == 0) or (s%2999 == 0):
                    plt.figure()
                    plt.plot(t, signal[0,0], 'b', alpha=0.7)
                    plt.plot(t, kappa[0,0].detach().numpy(), 'k', alpha=0.7)
                    plt.show()
                 
            self.update_bar(loss.item(), self.args)
            kappa.retain_grad()
        return estimates
