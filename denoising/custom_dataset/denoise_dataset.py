from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from EMCqMRI.core.base import base_dataset
from EMCqMRI.core.utilities import dataset_utilities
import numpy as np
import os
import pickle
import torch
import torchvision

class DatasetModel(base_dataset.Dataset):
    def __init__(self, configObject):
        super(DatasetModel, self).__init__(configObject)
        self.__name__ = 'Denoise Dataset'
        self.args = configObject.args
        self.data = []
        self.idx_control = -1
        if self.args.task.sigmaNoise >= 0:
            self.sigma = self.args.task.sigmaNoise
            self.sigmaDist = None
        elif self.args.task.sigmaNoise < 0:
            self.sigmaDist = torch.distributions.Uniform(0.0, 0.5)
        
    def get_synthetic_sin_function(self):
        t_in = np.arange(0, np.pi, 0.01)
        freq_rand = np.random.uniform(1,7) #self.args.task.frequency
        offset = 1.5
        freq = freq_rand*np.pi
        sin_base = np.sin(t_in*freq) + offset
        sin_image = np.repeat(np.expand_dims(sin_base, 0), repeats=50, axis=0)
        return sin_base

    def index_files(self):
        name_list = []
        for im in range(1000):
            name_list.append("File_MNIST_"+str(im))
        self.filename_list = name_list

    def add_noise_to_data(self, data):
        if self.sigmaDist:
            self.sigma = (self.sigmaDist.sample())
        training_signal = self.args.engine.likelihood_model.applyNoise(data, self.sigma)
        return training_signal

    def get_testing_data(self, idx):
        pass


    def get_label(self, idx):
        if not self.args.core.mode == 'testing':
            sin_image = self.get_synthetic_sin_function()
            self.training_label = torch.from_numpy(sin_image).type(torch.FloatTensor).to(self.args.engine.device)
            self.mask = [0]
        else:
            self.load_file(idx)
            self.training_label = self.data[0]['label'].squeeze()
            self.mask = [0]
            
        return self.training_label.unsqueeze(0), self.mask

    def get_signal(self, *local_args):
        if not self.args.core.mode == 'testing':
            clone_label = self.training_label.clone()
            training_signal = self.add_noise_to_data(clone_label)
            self.training_signal = training_signal.type(torch.FloatTensor).to(self.args.engine.device)
        else:
            self.training_signal = self.data[0]['signal'].squeeze()
        return self.training_signal.unsqueeze(0)

