from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from core.base import base_inference_model
from core.utilities.core_utilities import ProgressBarWrap
import torch
from scipy.sparse import diags
import numpy.linalg as alg

import matplotlib.pyplot as plt
import numpy as np

class Sobolev(base_inference_model.InferenceModel, object):
    """
    """
    def __init__(self, configObject):
        super(Sobolev, self).__init__()
        self.__name__ = 'Sobolev'
        self.__require_initial_guess__ = True
        self.args = configObject.args

    @ProgressBarWrap
    def update_bar(self, loss, args):
        return -1

    def forward(self, inputs):
        signal = inputs[0,0]
        b = signal
        n = len(signal)
        alpha = 80
        k = self.args.task.frequency
        
        b[0] = b[0]/2
        b[n-1] = b[n-1]/2

        T = diags([1/6, 2/3, 1/6], [-1, 0, 1], shape=(n, n)).toarray()
        T[0,0] = 1/3
        T[n-1,n-1] = 1/3

        S = diags([-1, 2, -1], [-1, 0, 1], shape=(n, n)).toarray()
        S[0,0] = 1
        S[n-1,n-1] = 1

        A = T + alpha * S
        u = alg.solve(A, b)

        # x = np.arange(0, np.pi, 0.01)
        # t = np.linspace(0,n,n)
        # plt.plot(t,signal)
        # plt.plot(t,u)
        # plt.plot(t, np.sin(k*x*np.pi)+1.5, c='k')
        # plt.legend(['f_delta', 'solution', 'ground_truth'])
        # plt.show()
        
        estimates = torch.from_numpy(u).unsqueeze(0).unsqueeze(0)
        return estimates
