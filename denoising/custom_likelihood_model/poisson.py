from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from core.base import base_likelihood_model
import torch
import math
import numpy as np


class Poisson(base_likelihood_model.Likelihood):
    """
        Class for the Poisson PDF.
        Methods:
            - logLikelihood
                inputs: signal (measured signal), mu (simulated signal) and sigma (SD of the noise)
                outputs: data consistency loss
            - applyNoise
                inputs: a signal and sigma
                outputs: Noisy signal corrupted by additive gaussian noise
    """
    
    def __init__(self, configObject):
        super(Poisson, self).__init__(configObject, self)
        self.__name__ = 'Poisson'
        self.args = configObject.args

    def logLikelihood(self, signal, modeled_signal):
        return torch.sum((signal - modeled_signal)**2)

    def applyNoise(self, signal, sigma):
        signal += torch.from_numpy(np.random.normal(0.0, sigma, signal.size())).to(self.args.engine.device)
        return signal

    