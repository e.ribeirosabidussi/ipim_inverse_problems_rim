import numpy as np
import pickle
import matplotlib.pyplot as plt

import os
import sys


def compute_bias(est_list, lab):
    bias = []
    for est in est_list:
        bias.append(np.mean(est - lab))
    median_bias = np.median(bias)
    std_bias = np.std(bias)
    print(median_bias, std_bias)

def compute_cv(est_list):
    sq_mean_est = np.mean(np.array(est_list)**2, 0)
    mean_sq_est = np.mean(est_list, 0)**2
    median_cv = np.median(sq_mean_est - mean_sq_est)
    std_cv = np.std(sq_mean_est - mean_sq_est)
    print(median_cv, std_cv)


if __name__ == '__main__':
    methods = ['analytical', 'analytical']
    estimates_path = './estimates/'
    snr = [5, 10, 30]
    freq = [1, 3, 7]

    for met_ in methods:
        for snr_ in snr:
            for freq_ in freq:
                print(met_, snr_, freq_)
                path = estimates_path + met_ + '/snr_' + str(snr_) + '_freq_' + str(freq_) + '/'
                estimated_list = []
                for file in os.listdir(path):
                    with open(path + file, 'rb') as f:
                        data = pickle.load(f)
                        label = data['labels'][0,0]
                        estimated = data['estimated']
                        estimated_list.append(estimated)

                compute_bias(estimated_list, label)
                compute_cv(estimated_list)
                