from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging

from EMCqMRI.core.configuration import pll_configuration
from EMCqMRI.core.engine import build_model as core_build
from EMCqMRI.core.engine import train_model

import torch
from torch.utils.tensorboard import SummaryWriter
writer = SummaryWriter('log_training/denoising_gpu')


import numpy as np
import matplotlib.pyplot as plt
import io
import PIL.Image
from torchvision.transforms import ToTensor


def override_model(configObject):

    from custom_dataset import denoise_dataset
    configObject.args.engine.dataset = denoise_dataset.DatasetModel(configObject)

    # from custom_inference_model.rim import rim
    # configObject.args.engine.inference_model = rim.Rim(configObject)

    from custom_inference_model.rim_stride import rim
    configObject.args.engine.inference_model = rim.Rim(configObject)

    from custom_likelihood_model import gaussian
    configObject.args.engine.likelihood_model = gaussian.Gaussian(configObject)

    from custom_signal_model import denoise
    configObject.args.engine.signal_model = denoise.Denoise(configObject)


def custom_log_fun(data, loss, sample_it, epoch):
    def read_convert_buffer_image(buffer):
        image = PIL.Image.open(buffer)
        image = ToTensor()(image)

        return image

    def get_plot_buf():
        plt.figure()
        plt.plot(t, data['signal'][0][0], 'b', alpha=0.3)
        plt.plot(t, data['estimated'][0], 'r', alpha=0.7)
        plt.plot(t, data['labels'][0], 'k')
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close()
        buf.seek(0)
        return buf

    iter_sample = (sample_it+1) + (1000*epoch)
    writer.add_scalar('Sample Loss', loss, iter_sample)

    t = np.linspace(0,len(data['labels'][0]), len(data['labels'][0]))

    if iter_sample % 50 == 0:
        buffer = get_plot_buf()
        image = read_convert_buffer_image(buffer)
        writer.add_image('Predictions', image, iter_sample)

        # fig, ax = plt.subplots(1,2)
        # ax[0].plot(t, data['signal'][0][0], 'b', alpha=0.3)
        # ax[0].plot(t, data['estimated'][0], 'r', alpha=0.7)
        # ax[0].plot(t, data['labels'][0], 'k')
        # ax[1].plot(data['labels'][0], data['estimated'][0], 'xk')

        # writer.scalar('', data['signal'][0][0], y)
        # plt.show()


def custom_log_fun_2d(data, loss, sample_it, epoch):
    # def read_convert_buffer_image(buffer):
    #     image = PIL.Image.open(buffer)
    #     image = ToTensor()(image)

    #     return image

    # def get_plot_buf():
    #     plt.figure()
    #     plt.imshow()
    #     plt.plot(t, data['signal'][0][0], 'b', alpha=0.3)
    #     plt.plot(t, data['estimated'][0], 'r', alpha=0.7)
    #     plt.plot(t, data['labels'], 'k')
    #     buf = io.BytesIO()
    #     plt.savefig(buf, format='png')
    #     plt.close()
    #     buf.seek(0)
    #     return buf

    iter_sample = (sample_it+1) + (100*epoch)
    # writer.add_scalar('Sample Loss', loss, iter_sample)

    t = np.linspace(0,len(data['labels']), len(data['labels']))
    print(iter_sample)
    if iter_sample % 200 == 0:
        # buffer = get_plot_buf()
        # image = read_convert_buffer_image(buffer)
        # writer.add_image('Predictions', image, iter_sample)

        fig, ax = plt.subplots(1,4)
        ax[0].imshow(data['signal'][0][0])
        ax[1].imshow(data['estimated'][0])
        ax[2].imshow(data['labels'][0])
        ax[3].plot(data['labels'][0], data['estimated'][0], 'kx', alpha=0.7)
        # ax[0].plot(t, data['estimated'], 'r', alpha=0.7)
        # ax[0].plot(t, data['labels'], 'k')
        # ax[1].plot(data['labels'], data['estimated'], 'xk')

        # writer.scalar('', data['signal'][0][0], y)
        plt.show()



if __name__=='__main__':
    configurationObj = pll_configuration.Configuration('training')
    override_model(configurationObj)
    core_build.make(configurationObj)
    
    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))

    configurationObj.args.engine.trainer.log_training_fun = custom_log_fun

    logging.info('Starting training...')
    configurationObj.args.engine.trainer.run()
