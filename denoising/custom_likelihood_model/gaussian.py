from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from EMCqMRI.core.base import base_likelihood_model
import torch
import math
import numpy as np


class Gaussian(base_likelihood_model.Likelihood, torch.nn.modules.loss._Loss):
    """
        Class for the Gaussian PDF.
        Methods:
            - logLikelihood
                inputs: signal (measured signal), mu (simulated signal) and sigma (SD of the noise)
                outputs: data consistency loss
            - applyNoise
                inputs: a signal and sigma
                outputs: Noisy signal corrupted by additive gaussian noise
    """
    
    def __init__(self, configObject):
        super(Gaussian, self).__init__(configObject, self)
        self.__name__ = 'Gaussian'
        self.args = configObject.args

    def tv_loss(self, x):
        diff_1 = torch.roll(x, -1, 0) - x
        diff_2 = torch.roll(x, -1, 1) - x if len(x)>1 else 0
        grad_norm1 = diff_1**2 + diff_2**2 + 0.00000000001
        norm = torch.sum(torch.sqrt(grad_norm1))
        return norm

    def tik_loss(self, x):
        diff_1 = torch.roll(x, -1, 0) - x
        diff_2 = torch.roll(x, -1, 1) - x if len(np.shape(x))>1 else 0
        grad_norm1 = diff_1**2 + diff_2**2 + 0.00000000001
        norm = torch.sum(grad_norm1**2)
        return norm

    def logLikelihood(self, signal, modeled_signal):
        error = torch.sum((signal - modeled_signal)**2)
        if self.args.task.applyRegularization:
            lambda_value = self.args.task.regularizerStrength
            regularizer = lambda_value*self.tik_loss(modeled_signal[0,0])
            return error + regularizer
        else:
            return error

    def forward(self, signal, modeled_signal):
        return self.logLikelihood(signal, modeled_signal)

    def applyNoise(self, signal, sigma):
        signal += torch.from_numpy(np.random.normal(0.0, sigma, signal.size())).to(self.args.engine.device)
        return signal

    