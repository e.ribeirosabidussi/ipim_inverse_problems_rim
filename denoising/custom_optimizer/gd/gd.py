from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from core.base import base_inference_model
from core.utilities.core_utilities import ProgressBarWrap
import torch

import matplotlib.pyplot as plt
import numpy as np

class GradientDescent(base_inference_model.InferenceModel, object):
    """
    """
    def __init__(self, configObject):
        super(GradientDescent, self).__init__()
        self.__name__ = 'GradientDescent'
        self.__require_initial_guess__ = True
        self.args = configObject.args

    def forward(self, data, loss):
        loss.backward()
        data_ = data - (self.args.engine.learningRate*data.grad)
        return data_
