from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from core.base import base_signal_model
import torch


class Denoise(base_signal_model.SignalModel):
    """
        Class implementing the forward model of a Look Locker based acquisition.
        Contains methods:
            - setTau: set inversion times of acquisition
            - forwardModel: Defines the signal model
            - generateWeightedImages: wrapper to generate N weighted images from A, B and T1 maps
            - gradients: Uses autograd to automatically compute gradients w.r.t. the likelihood function
            - initializeParameters: Initialises all tissue parameters 
    """
    def __init__(self, configObject):
        super(Denoise, self).__init__()
        self.__name__ = 'Denoise'
        self.__nParameters__ = 1
        self.args = configObject.args

    def forwardModel(self, kappa, *args):
        kappa = 1 * kappa
        return kappa

    def initializeParameters(self, signal):
        signal_ = signal.clone()
        initial_kappa = torch.autograd.Variable((signal_).to(device=self.args.engine.device), requires_grad=True)
        return initial_kappa

        