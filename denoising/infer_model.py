from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging

from EMCqMRI.core.configuration import pll_configuration
from EMCqMRI.core.engine import build_model as core_build
from EMCqMRI.core.engine import estimate
from EMCqMRI.core.utilities import image_utilities

def override_model(configObject):

    from custom_dataset import denoise_dataset
    configObject.args.engine.dataset = denoise_dataset.DatasetModel(configObject)

    if configObject.args.engine.inferenceModel == 'rim':
        from custom_inference_model.rim import rim
        configObject.args.engine.inference_model = rim.Rim(configObject)

    elif configObject.args.engine.inferenceModel == 'mle':
        from custom_inference_model.mle import mle
        from custom_optimizer.gd import gd
        configObject.args.engine.inference_model = mle.Mle(configObject)
        configObject.args.engine.optimizer = gd.GradientDescent(configObject)

    elif configObject.args.engine.inferenceModel == 'sobolev':
        from custom_inference_model.sobolev import RegSobolev
        configObject.args.engine.inference_model = RegSobolev.Sobolev(configObject)

    from custom_likelihood_model import gaussian
    configObject.args.engine.likelihood_model = gaussian.Gaussian(configObject)

    from custom_signal_model import denoise
    configObject.args.engine.signal_model = denoise.Denoise(configObject)


if __name__=='__main__':
    configurationObj = pll_configuration.Configuration('testing')
    override_model(configurationObj)
    core_build.make(configurationObj)
    
    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))
    logging.info('Starting inference...')

    # for i, sample in enumerate(configurationObj.args.engine.dataloader):
    #     print(i)
    #     print(sample['image'].shape)
    #     print(sample['label'].shape)
    
    estimated_data = configurationObj.args.engine.estimator.run(return_result = True)
    # print(estimated_data)
